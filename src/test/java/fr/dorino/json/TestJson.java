
package fr.dorino.json;

import java.io.StringReader;
import java.io.StringWriter;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


/**
 *
 * @author eric
 */
public class TestJson {

    @BeforeAll
    public static void setUpClass() throws Exception {

    }

    @AfterAll
    public static void tearDownClass() throws Exception {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void jsonString1() throws Exception {
        JsonString json = new JsonString("Hello");
        assertTrue("\"Hello\"".equals(json.toString()));
        assertTrue("Hello".equals(json.getString()));
        assertTrue("Hello".equals(json.getChars()));
    }

    @Test
    public void jsonString2() throws Exception {
        JsonString json = new JsonString("Hello'single quote");
        assertTrue("\"Hello'single quote\"".equals(json.toString()));
        assertTrue("Hello'single quote".equals(json.getString()));
        assertTrue("Hello'single quote".equals(json.getChars()));
    }

    @Test
    public void jsonString3() throws Exception {
        JsonString json = new JsonString("Hello \"double quote\"!");
        assertTrue("\"Hello \\\"double quote\\\"!\"".equals(json.toString()));
        assertTrue("Hello \"double quote\"!".equals(json.getString()));
        assertTrue("Hello \"double quote\"!".equals(json.getChars()));
    }

    @Test
    public void jsonString4() throws Exception {
        JsonString json = new JsonString("Hello\u0020espace");
        assertTrue("\"Hello espace\"".equals(json.toString()));
        assertTrue("Hello espace".equals(json.getString()));
        assertTrue("Hello espace".equals(json.getChars()));
    }

    @Test
    public void jsonString5() throws Exception {
        JsonString json = new JsonString("Hello\r\nsaut de ligne");
        assertTrue("\"Hello\\r\\nsaut de ligne\"".equals(json.toString()));
        assertTrue("Hello\r\nsaut de ligne".equals(json.getString()));
        assertTrue("Hello\r\nsaut de ligne".equals(json.getChars()));

    }

    @Test
    public void jsonString6() throws Exception {
        JsonString json = new JsonString("Hello\u0009Tab");
        assertTrue("\"Hello\\tTab\"".equals(json.toString()));
        assertTrue("Hello\tTab".equals(json.getString()));
        assertTrue("Hello\u0009Tab".equals(json.getString()));
        assertTrue("Hello\tTab".equals(json.getChars()));
        assertTrue("Hello\u0009Tab".equals(json.getChars()));
    }

    @Test
    public void jsonString7() throws Exception {
        JsonString json = new JsonString("Hello\u000cFormFeed");
        assertTrue("\"Hello\\fFormFeed\"".equals(json.toString()));
        assertTrue("Hello\fFormFeed".equals(json.getString()));
        assertTrue("Hello\u000cFormFeed".equals(json.getString()));
        assertTrue("Hello\fFormFeed".equals(json.getChars()));
        assertTrue("Hello\u000cFormFeed".equals(json.getChars()));
    }

    @Test
    public void jsonString8() throws Exception {
        JsonString json = new JsonString("Hello\u0007Bell");
        assertTrue("\"Hello\\u0007Bell\"".equals(json.toString()));
        assertTrue("Hello\u0007Bell".equals(json.getString()));
        assertTrue("Hello\u0007Bell".equals(json.getChars()));
    }

    @Test
    public void jsonNumber1() throws Exception {
        JsonNumber jsonInt = new JsonNumber(42);
        assertTrue(
                jsonInt.isIntegral() &&
                42 == jsonInt.intValue());
    }

    @Test
    public void jsonNumber2() throws Exception {
        JsonNumber jsonInt = new JsonNumber(42.5);
        assertTrue(
                !jsonInt.isIntegral() &&
                42.5 == jsonInt.doubleValue());
    }

    @Test
    public void jsonArray1() throws Exception {
        JsonReader reader = Json.createReader(new StringReader("[]"));
        JsonArray array = (JsonArray) reader.readArray();
        assertTrue(array.isEmpty());
    }

    @Test
    public void jsonArray2() throws Exception {
        JsonReader reader = Json.createReader(new StringReader("[1,2,3,null,true,false,\"Hello\", \"le\", \"monde\"]"));
        JsonArray array = (JsonArray) reader.readArray();
        assertTrue(array.getInt(0) == 1);
        assertTrue(array.isNull(3));
        assertTrue(array.getBoolean(4));
        assertTrue(array.getString(6).equals("Hello"));
    }

    @Test
    public void jsonObject1() throws Exception {
        JsonReader reader = Json.createReader(new StringReader("{}"));
        javax.json.JsonObject obj = reader.readObject();
        assertTrue(obj.isEmpty());
    }

    @Test
    public void jsonObject2() throws Exception {
        JsonReader reader = Json.createReader(new StringReader("{\"one\":1,\"x\":\"X\u0020X\"}"));
        JsonObject obj = (JsonObject) reader.readObject();
        assertTrue(obj.getInt("one") == 1);
        assertTrue(obj.getString("x").equals("X X"));
    }

    @Test
    public void jsonBuilder1() throws Exception {
        JsonObject value = Json.createObjectBuilder()
        .add("firstName", "John")
        .add("lastName", "Smith")
        .add("age", 25)
        .add("address", Json.createObjectBuilder()
            .add("streetAddress", "21 2nd Street")
            .add("city", "New York")
            .add("state", "NY")
            .add("postalCode", "10021"))
         .add("phoneNumber", Json.createArrayBuilder()
            .add(Json.createObjectBuilder()
                .add("type", "home")
                .add("number", "212 555-1234"))
            .add(Json.createObjectBuilder()
                .add("type", "fax")
                .add("number", "646 555-4567")))
         .build();
        StringWriter out = new StringWriter();
        JsonWriter writer = new JsonWriter(out);
        writer.write(value);
        System.out.println(out.toString());
        assertTrue(
            "{\"firstName\":\"John\",\"lastName\":\"Smith\",\"address\":{\"streetAddress\":\"21 2nd Street\",\"city\":\"New York\",\"postalCode\":\"10021\",\"state\":\"NY\"},\"phoneNumber\":[{\"number\":\"212 555-1234\",\"type\":\"home\"},{\"number\":\"646 555-4567\",\"type\":\"fax\"}],\"age\":25}"
                .equals(out.toString())
        );
    }

    @Test
    public void jsonBuilder2() throws Exception {
    JsonArray value = Json.createArrayBuilder()
        .add(Json.createObjectBuilder()
            .add("type", "home")
            .add("number", "212 555-1234"))
        .add(Json.createObjectBuilder()
            .add("type", "fax")
            .add("number", "646 555-4567"))
        .build();
        StringWriter out = new StringWriter();
        JsonWriter writer = new JsonWriter(out);
        writer.write(value);
        System.out.println(out.toString());
        assertTrue(
            "[{\"number\":\"212 555-1234\",\"type\":\"home\"},{\"number\":\"646 555-4567\",\"type\":\"fax\"}]"
            .equals(out.toString())
        );
    }
}
