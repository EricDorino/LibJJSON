/*
 * Copyright 2021 eric.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.dorino.json;

import fr.dorino.json.parser.Parser;
import java.io.IOException;
import java.io.Reader;
import javax.json.JsonArray;
import javax.json.JsonException;
import javax.json.JsonObject;
import javax.json.JsonStructure;
import javax.json.JsonValue;
import javax.json.JsonValue.ValueType;
import javax.json.stream.JsonParsingException;

/**
 *
 * @author eric
 */
public class JsonReader implements javax.json.JsonReader {

    private final Reader reader;

    public JsonReader(Reader reader) {
        this.reader = reader;
    }

    @Override
    public JsonStructure read() throws JsonException, JsonParsingException {
        Parser parser = new Parser(false);
        JsonValue result = parser.run(reader);
        if (result.getValueType() == ValueType.ARRAY ||
            result.getValueType() == ValueType.OBJECT)
            return (JsonStructure)result;
        else throw new JsonException("Not a JSON array or JSON object");
    }

    @Override
    public JsonObject readObject() throws JsonException, JsonParsingException {
        Parser parser = new Parser(false);
        JsonValue result = parser.run(reader);
        if (result.getValueType() == ValueType.OBJECT)
            return result.asJsonObject();
        else throw new JsonException("Not a JSON object");
    }

    @Override
    public JsonArray readArray() throws JsonException, JsonParsingException {
        Parser parser = new Parser(false);
        JsonValue result = parser.run(reader);
        if (result.getValueType() == ValueType.ARRAY)
            return result.asJsonArray();
        else throw new JsonException("Not a JSON array");
    }

    @Override
    public void close() throws JsonException {
        try {
            reader.close();
        } catch (IOException e) {
            throw new JsonException(e.getMessage(), e.getCause());
        }
    }

}
