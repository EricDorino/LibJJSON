/*
 * Copyright 2021 eric.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.dorino.json;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.json.JsonValue;

/**
 *
 * @author eric
 */
public class JsonObject implements javax.json.JsonObject {

    private final Map<String, JsonValue> values;

    public JsonObject(Map<String, JsonValue> values) {
        this.values = values;
    }

    public JsonObject(List<? extends Map.Entry<String, JsonValue>> values) {
        this.values = new HashMap<>(values.size());
        for (Map.Entry<String, JsonValue> entry : values) {
            this.values.put(entry.getKey(), entry.getValue());
        }
    }

    public JsonObject() {
        this(new ArrayList<>());
    }

    @Override
    public JsonArray getJsonArray(String name) {
        JsonValue value = get(name);
        if (value == null)
            return null;
        else if (value.getValueType() == ValueType.ARRAY)
            return (JsonArray)value;
        else
            throw new ClassCastException();
    }

    @Override
    public JsonObject getJsonObject(String name) {
        JsonValue value = get(name);
        if (value == null)
            return null;
        else if (value.getValueType() == ValueType.OBJECT)
            return (JsonObject)value;
        else
            throw new ClassCastException();
    }

    @Override
    public JsonNumber getJsonNumber(String name) {
        JsonValue value = get(name);
        if (value == null)
            return null;
        else if (value.getValueType() == ValueType.NUMBER)
            return (JsonNumber)value;
        else
            throw new ClassCastException();
    }

    @Override
    public JsonString getJsonString(String name) {
        JsonValue value = get(name);
        if (value == null)
            return null;
        else if (value.getValueType() == ValueType.STRING)
            return (JsonString)value;
        else
            throw new ClassCastException();
    }

    @Override
    public String getString(String name) {
        return getJsonString(name).getString();
    }

    @Override
    public String getString(String name, String defaultValue) {
        try {
            return getJsonString(name).getString();
        } catch (NullPointerException | ClassCastException e) {
            return defaultValue;
        }
    }

    @Override
    public int getInt(String name) {
        return getJsonNumber(name).intValue();
    }

    @Override
    public int getInt(String name, int defaultValue) {
        try {
            return getJsonNumber(name).intValue();
        } catch (NullPointerException | ClassCastException e) {
            return defaultValue;
        }
    }

    @Override
    public boolean getBoolean(String name) {
        JsonValue value = get(name);
        if (value.getValueType() == ValueType.TRUE)
            return true;
        else if (value.getValueType() == ValueType.FALSE)
            return false;
        throw new ClassCastException();
    }

    @Override
    public boolean getBoolean(String name, boolean defaultValue) {
        try {
            return getBoolean(name);
        } catch (NullPointerException | ClassCastException e) {
            return defaultValue;
        }
    }

    @Override
    public boolean isNull(String name) {
        return get(name).getValueType() == ValueType.NULL;
    }

    @Override
    public ValueType getValueType() {
        return ValueType.OBJECT;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder().append('{');
        Iterator<Map.Entry<String, JsonValue>> iter = values.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry<String, JsonValue> entry = iter.next();
            result.append('"');
            result.append(entry.getKey());
            result.append('"');
            result.append(':');
            result.append(entry.getValue().toString());
            if (iter.hasNext())
                result.append(',');
        }
        return result.append('}').toString();
    }

    @Override
    public int size() {
        return this.values.size();
    }

    @Override
    public boolean isEmpty() {
        return this.values.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return this.values.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return this.values.containsValue(value);
    }

    @Override
    public JsonValue get(Object key) {
        return this.values.get(key);
    }

    @Override
    public JsonValue put(String key, JsonValue value) {
        return this.values.put(key, value);
    }

    @Override
    public JsonValue remove(Object key) {
        return this.values.remove(key);
    }

    @Override
    public void putAll(Map<? extends String, ? extends JsonValue> map) {
        this.values.putAll(map);
    }

    @Override
    public void clear() {
        this.values.clear();
    }

    @Override
    public Set<String> keySet() {
        return this.values.keySet();
    }

    @Override
    public Collection<JsonValue> values() {
        return this.values.values();
    }

    @Override
    public Set<Entry<String, JsonValue>> entrySet() {
        return this.values.entrySet();
    }

}
