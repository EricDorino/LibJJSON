/*
 * Copyright 2021 eric.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.dorino.json;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

/**
 *
 * @author eric
 */
public class JsonArrayBuilder implements javax.json.JsonArrayBuilder {

    public List<JsonValue> list;

    public JsonArrayBuilder() {
        this.list = new ArrayList<>();
    }

    @Override
    public JsonArrayBuilder add(JsonValue value) {
        this.list.add(value);
        return this;
    }

    @Override
    public JsonArrayBuilder add(String value) {
        this.list.add(new JsonString(value));
        return this;
    }

    @Override
    public JsonArrayBuilder add(BigDecimal value) {
        this.list.add(new JsonNumber(value));
        return this;
    }

    @Override
    public JsonArrayBuilder add(BigInteger value) {
        this.list.add(new JsonNumber(value));
        return this;
    }

    @Override
    public JsonArrayBuilder add(int value) {
        this.list.add(new JsonNumber(value));
        return this;
    }

    @Override
    public JsonArrayBuilder add(long value) {
        this.list.add(new JsonNumber(value));
        return this;
    }

    @Override
    public JsonArrayBuilder add(double value) {
        this.list.add(new JsonNumber(value));
        return this;
    }

    @Override
    public JsonArrayBuilder add(boolean value) {
        this.list.add(value ? JsonValue.TRUE : JsonValue.FALSE);
        return this;
    }

    @Override
    public JsonArrayBuilder addNull() {
        this.list.add(JsonValue.NULL);
        return this;
    }

    @Override
    public JsonArrayBuilder add(JsonObjectBuilder value) {
        if (value == null)
            throw new NullPointerException();
        this.list.add(value.build());
        return this;
    }

    @Override
    public JsonArrayBuilder add(javax.json.JsonArrayBuilder value) {
        if (value == null)
            throw new NullPointerException();
        this.list.add(value.build());
        return this;
    }

    @Override
    public JsonArray build() {
        return new JsonArray(list);
    }
}
