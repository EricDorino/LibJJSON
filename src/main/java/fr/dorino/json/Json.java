/*
 * Copyright 2021 eric.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.dorino.json;

import java.io.Reader;
import java.io.Writer;

/**
 *
 * @author eric
 */
public class Json {

    public static JsonArrayBuilder createArrayBuilder() {
        return new JsonArrayBuilder();
    }

    public static JsonObjectBuilder createObjectBuilder() {
        return new JsonObjectBuilder();
    }

    public static JsonReader createReader(Reader reader) {
        return new JsonReader(reader);
    }

    public static JsonWriter createWriter(Writer writer) {
        return new JsonWriter(writer);
    }

}
