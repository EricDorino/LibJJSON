/*
 * Copyright 2021 eric.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.dorino.json;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import javax.json.JsonArrayBuilder;
import javax.json.JsonValue;

/**
 *
 * @author eric
 */
public class JsonObjectBuilder implements javax.json.JsonObjectBuilder {

    private final Map<String, JsonValue> map;

    public JsonObjectBuilder() {
        this.map = new HashMap<>();
    }

    @Override
    public JsonObjectBuilder add(String name, JsonValue value) {
        this.map.put(name, value);
        return this;
    }

    @Override
    public JsonObjectBuilder add(String name, String value) {
        this.map.put(name, new JsonString(value));
        return this;
    }

    @Override
    public JsonObjectBuilder add(String name, BigInteger value) {
        this.map.put(name, new JsonNumber(value));
        return this;
    }

    @Override
    public JsonObjectBuilder add(String name, BigDecimal value) {
        this.map.put(name, new JsonNumber(value));
        return this;
    }

    @Override
    public JsonObjectBuilder add(String name, int value) {
        this.map.put(name, new JsonNumber(value));
        return this;
    }

    @Override
    public JsonObjectBuilder add(String name, long value) {
        this.map.put(name, new JsonNumber(value));
        return this;
    }

    @Override
    public JsonObjectBuilder add(String name, double value) {
        this.map.put(name, new JsonNumber(value));
        return this;
    }

    @Override
    public JsonObjectBuilder add(String name, boolean value) {
        this.map.put(name, value ? JsonValue.TRUE : JsonValue.FALSE);
        return this;
    }

    @Override
    public JsonObjectBuilder addNull(String name) {
        this.map.put(name, JsonValue.NULL);
        return this;
    }

    @Override
    public JsonObjectBuilder add(String name, javax.json.JsonObjectBuilder value) {
        if (value == null)
            throw new NullPointerException();
        this.map.put(name, value.build());
        return this;
    }

    @Override
    public JsonObjectBuilder add(String name, JsonArrayBuilder value) {
        if (value == null)
            throw new NullPointerException();
        this.map.put(name, value.build());
        return this;
    }

    @Override
    public JsonObject build() {
        return new JsonObject(map);
    }

}
