/*
 * Copyright 2021 eric.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.dorino.json;

import java.awt.event.KeyEvent;
import java.util.Objects;

/**
 *
 * @author eric
 */
public class JsonString implements javax.json.JsonString {

    private final String value;

    public JsonString(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        else if (obj instanceof JsonString) {
            if (this == obj)
                return true;
            else
                return this.value.equals(((JsonString)obj).value);
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 11 * hash + Objects.hashCode(this.value);
        return hash;
    }

    @Override
    public CharSequence getChars() { return value; }

    @Override
    public ValueType getValueType() { return ValueType.STRING; }

    private static boolean isPrintable(char c) {
        Character.UnicodeBlock block = Character.UnicodeBlock.of(c);
        return !Character.isISOControl(c) &&
               c != KeyEvent.CHAR_UNDEFINED &&
               block != null && block != Character.UnicodeBlock.SPECIALS;
    }

    public static String formatString(String str) {
        StringBuilder result = new StringBuilder().append('"');
        for (int i = 0; i < str.length(); i++) {
            char c;
            switch (c = str.charAt(i)) {
                case '"':
                    result.append("\\\"");
                    break;
                case '\\':
                    result.append("\\\\");
                    break;
                case '\b':
                    result.append("\\b");
                    break;
                case '\f':
                    result.append("\\f");
                    break;
                case '\n':
                    result.append("\\n");
                    break;
                case '\r':
                    result.append("\\r");
                    break;
                case '\t':
                    result.append("\\t");
                    break;
                default:
                    if (isPrintable(c))
                        result.append(c);
                    else {
                        char[] a = new char[1];
                        a[0] = c;
                        result.append(String.format("\\u%04x", Character.codePointAt(a, 0)));
                    }
                    break;
            }
        }
        return result.append('"').toString();
    }

    @Override
    public String getString() { return value; }

    @Override
    public String toString() { return formatString(value); }
}
