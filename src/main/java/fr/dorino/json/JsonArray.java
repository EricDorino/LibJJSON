/*
 * Copyright 2021 eric.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.dorino.json;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import javax.json.JsonValue;

/**
 *
 * @author eric
 */
public class JsonArray implements javax.json.JsonArray {

    private final List<JsonValue> values;

    public JsonArray(List<JsonValue> values) {
        this.values = values;
    }

    public JsonArray() {
        this(new ArrayList<>());
    }

    @Override
    public JsonObject getJsonObject(int index) {
        JsonValue value = get(index);
        if (value.getValueType() == ValueType.OBJECT)
            return (JsonObject)value;
        throw new ClassCastException();
    }

    @Override
    public javax.json.JsonArray getJsonArray(int index) {
        JsonValue value = get(index);
        if (value.getValueType() == ValueType.ARRAY)
            return (JsonArray)value;
        throw new ClassCastException();
    }

    @Override
    public JsonNumber getJsonNumber(int index) {
        JsonValue value = get(index);
        if (value.getValueType() == ValueType.NUMBER)
            return (JsonNumber)value;
        throw new ClassCastException();
    }

    @Override
    public JsonString getJsonString(int index) {
        JsonValue value = get(index);
        if (value.getValueType() == ValueType.STRING)
            return (JsonString)value;
        throw new ClassCastException();
    }

    @Override
    public <T extends JsonValue> List<T> getValuesAs(Class<T> clazz) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getString(int index) {
        return getJsonString(index).getString();
    }

    @Override
    public String getString(int index, String defaultValue) {
        try {
            return getJsonString(index).getString();
        } catch (IndexOutOfBoundsException | ClassCastException e) {
            return defaultValue;
        }
    }

    @Override
    public int getInt(int index) {
        return getJsonNumber(index).intValue();
    }

    @Override
    public int getInt(int index, int defaultValue) {
        try {
            return getJsonNumber(index).intValue();
        } catch (IndexOutOfBoundsException | ClassCastException e) {
            return defaultValue;
        }
    }

    @Override
    public boolean getBoolean(int index) {
        JsonValue value = get(index);
        if (value.getValueType() == ValueType.TRUE)
            return true;
        else if (value.getValueType() == ValueType.FALSE)
            return false;
        throw new ClassCastException();
    }

    @Override
    public boolean getBoolean(int index, boolean defaultValue) {
        try {
            return getBoolean(index);
        } catch (IndexOutOfBoundsException | ClassCastException e) {
            return defaultValue;
        }
    }

    @Override
    public boolean isNull(int index) {
        return get(index).getValueType() == ValueType.NULL;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder().append('[');
        Iterator<JsonValue> iter = values.iterator();
        while (iter.hasNext()) {
            result.append(iter.next().toString());
            if (iter.hasNext())
                result.append(',');
        }
        return result.append(']').toString();
    }


    @Override
    public ValueType getValueType() { return ValueType.ARRAY;  }

    @Override
    public int size() { return this.values.size(); }

    @Override
    public boolean isEmpty() { return this.values.isEmpty(); }

    @Override
    public boolean contains(Object obj) {
        return this.values.contains(obj);
    }

    @Override
    public Iterator<JsonValue> iterator() {
        return this.values.iterator();
    }

    @Override
    public Object[] toArray() {
        return this.values.toArray();
    }

    @Override
    public <T> T[] toArray(T[] values) {
        return this.values.toArray(values);
    }

    @Override
    public boolean add(JsonValue value) {
        return this.values.add(value);
    }

    @Override
    public boolean remove(Object obj) {
        return this.values.remove(obj);
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return this.values.containsAll(collection);
    }

    @Override
    public boolean addAll(Collection<? extends JsonValue> collection) {
        return this.values.addAll(collection);
    }

    @Override
    public boolean addAll(int index, Collection<? extends JsonValue> collection) {
        return this.values.addAll(index, collection);
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        return this.values.removeAll(collection);
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        return this.values.retainAll(collection);
    }

    @Override
    public void clear() {
        this.values.clear();
    }

    @Override
    public JsonValue get(int index) {
        return this.values.get(index);
    }

    @Override
    public JsonValue set(int index, JsonValue value) {
        return this.values.set(index, value);
    }

    @Override
    public void add(int index, JsonValue value) {
        this.values.add(index, value);
    }

    @Override
    public JsonValue remove(int index) {
        return this.values.remove(index);
    }

    @Override
    public int indexOf(Object obj) {
        return this.values.indexOf(obj);
    }

    @Override
    public int lastIndexOf(Object obj) {
        return this.values.lastIndexOf(obj);
    }

    @Override
    public ListIterator<JsonValue> listIterator() {
        return this.values.listIterator();
    }

    @Override
    public ListIterator<JsonValue> listIterator(int obj) {
        return this.values.listIterator(obj);
    }

    @Override
    public List<JsonValue> subList(int firstIndex, int lastIndex) {
        return this.values.subList(firstIndex, lastIndex);
    }

}
