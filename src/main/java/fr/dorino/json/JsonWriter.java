/*
 * Copyright 2021 eric.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.dorino.json;

import java.io.IOException;
import java.io.Writer;
import javax.json.JsonArray;
import javax.json.JsonException;
import javax.json.JsonObject;
import javax.json.JsonStructure;

/**
 *
 * @author eric
 */
public class JsonWriter implements javax.json.JsonWriter {

    private final Writer writer;

    public JsonWriter(Writer writer) {
        this.writer = writer;
    }

    @Override
    public void writeArray(JsonArray value) {
        try {
            writer.write(value.toString());
        } catch (IOException e) {
            throw new JsonException(e.getMessage(), e.getCause());
        }
    }

    @Override
    public void writeObject(JsonObject value) {
        try {
            writer.write(value.toString());
        } catch (IOException e) {
            throw new JsonException(e.getMessage(), e.getCause());
        }
    }

    @Override
    public void write(JsonStructure value) {
        try {
            writer.write(value.toString());
        } catch (IOException e) {
            throw new JsonException(e.getMessage(), e.getCause());
        }
    }

    @Override
    public void close() throws JsonException {
        try {
            writer.close();
        } catch (IOException e) {
            throw new JsonException(e.getMessage(), e.getCause());
        }
    }

}
