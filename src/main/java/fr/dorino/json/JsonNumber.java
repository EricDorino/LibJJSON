/*
 * Copyright 2021 eric.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.dorino.json;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Objects;

/**
 *
 * @author eric
 */
public class JsonNumber implements javax.json.JsonNumber {

    private final BigDecimal value;

    public JsonNumber(BigDecimal value) {
        this.value = value;
    }

    public JsonNumber(int value) {
        this.value = new BigDecimal(value);
    }

    public JsonNumber(long value) {
        this.value = new BigDecimal(value);
    }

    public JsonNumber(BigInteger value) {
        this.value = new BigDecimal(value);
    }

    public JsonNumber(double value) {
        this.value = new BigDecimal(value);
    }

    public JsonNumber(String value) {
        this.value = new BigDecimal(value);
    }

    @Override
    public boolean isIntegral() { return this.value.scale() == 0; }

    @Override
    public int intValue() { return this.value.intValue(); }

    @Override
    public int intValueExact() { return this.value.intValueExact(); }

    @Override
    public long longValue() { return this.value.longValue(); }

    @Override
    public long longValueExact() { return this.value.longValueExact(); }

    @Override
    public BigInteger bigIntegerValue() { return this.value.toBigInteger(); }

    @Override
    public BigInteger bigIntegerValueExact() { return this.value.toBigIntegerExact(); }

    @Override
    public double doubleValue() { return this.value.doubleValue(); }

    @Override
    public BigDecimal bigDecimalValue() { return this.value; }

    @Override
    public ValueType getValueType() { return ValueType.NUMBER; }

    @Override
    public String toString() { return this.value.toString(); }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        else if (obj instanceof JsonNumber) {
            if (this == obj)
                return true;
            else
                return this.value.equals(((JsonNumber)obj).value);
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.value);
        return hash;
    }
}
