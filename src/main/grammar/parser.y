/*
 * Copyright (C) 2012,  Eric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

%{

import fr.dorino.json.JsonArray;
import fr.dorino.json.JsonObject;
import fr.dorino.json.JsonNumber;
import fr.dorino.json.JsonString;
import fr.dorino.json.JsonPair;
import fr.dorino.json.stream.JsonLocation;
import javax.json.JsonValue;
import javax.json.stream.JsonParsingException;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

%}


%token <sval> STRING
%token <ival> INT
%token <dval> REAL
%token TRUE FALSE NULL

%type <obj> string
%type <obj> array object value
%type <obj> pair_list
%type <obj> pair
%type <obj> value_list

%{

%}

%%

json: object { result = (JsonValue)$1; }
    | array  { result = (JsonValue)$1; }
    ;

object: '{' pair_list '}'	{ $$ = new JsonObject((List<JsonPair>)$2); }
	| '{' '}'				{ $$ = new JsonObject(); }
	;

pair_list: pair_list ',' pair   {
            $$ = ((List<JsonPair>)$1);
            ((List<JsonPair>)$$).add((JsonPair)$3);
        }
	| pair {
            $$ = new ArrayList<JsonPair>();
            ((List<JsonPair>)$$).add((JsonPair)$1);
        }
	;

pair: string ':' value	{ $$ = new JsonPair(((JsonString)$1).getString(), (JsonValue)$3); }
	;

array: '[' value_list ']'	{ $$ = new JsonArray((List<JsonValue>)$2); }
	| '[' ']'				{ $$ = new JsonArray(); }

value_list: value_list ',' value	{
            $$ = ((List<JsonValue>)$1);
            ((List<JsonValue>)$$).add((JsonValue)$3);
        }
	| value	{
            $$ = new ArrayList<JsonValue>();
            ((List<JsonValue>)$$).add((JsonValue)$1);
        }
	;

value: object
	| array
	| string
	| int		{ $$ = new JsonNumber(yylval.ival); }
	| real		{ $$ = new JsonNumber(yylval.dval); }
	| TRUE		{ $$ = JsonValue.TRUE; }
    | FALSE		{ $$ = JsonValue.FALSE; }
	| NULL		{ $$ = JsonValue.NULL; }
	;

string: STRING  { $$ = new JsonString(yylval.sval); };

int: INT
	;

real: REAL
	;

%%

static Lex lex;
static JsonValue result;

public JsonValue run(Reader in) throws JsonParsingException {
    lex = new Lex(in, this);
    yyparse();
    return result;
}

int yylex() throws JsonParsingException {
    try {
        return lex.yylex();
    } catch (IOException e) {
        throw new JsonParsingException(
            e.getMessage(), e.getCause(),
            new JsonLocation(lex.getPosition()));
    }
}

void yyerror(String message) throws JsonParsingException {
    throw new JsonParsingException(
        message,
        new JsonLocation(lex.getPosition()));

}