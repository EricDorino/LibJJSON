
package fr.dorino.json.parser;

import fr.dorino.json.stream.JsonLocation;
import javax.json.stream.JsonParsingException;

%%

%byaccj
%class Lex
%unicode
%state STRING_BEGIN
%yylexthrow JsonParsingException
%char

%{

private Parser parser;
private StringBuilder sb = new StringBuilder();

public Lex(java.io.Reader in, Parser parser) {
    this(in);
    this.parser = parser;
}

long getPosition() { return yychar; }

%}

HexDigit = [a-fA-F0-9]
Int = [-]?[0-9]+
Real = {Int}((\.[0-9]+)?([eE][-+]?[0-9]+)?)
WhiteSpace = [ \t\r\n]
Unescaped = [^\"\\]

%%

<STRING_BEGIN> \"   {
    parser.yylval = new ParserVal(sb.toString());
    yybegin(YYINITIAL);
    sb = null;
    return Parser.STRING;
}
<STRING_BEGIN>      { Unescaped}+	{ sb.append(yytext()); }
<STRING_BEGIN> \\\" { sb.append('"'); }
<STRING_BEGIN> \\\\ { sb.append('\\'); }
<STRING_BEGIN> \\\/ { sb.append('/'); }
<STRING_BEGIN> \\b  { sb.append('\b'); }
<STRING_BEGIN> \\f  { sb.append('\f'); }
<STRING_BEGIN> \\n  { sb.append('\n'); }
<STRING_BEGIN> \\r  { sb.append('\r'); }
<STRING_BEGIN> \\t  { sb.append('\t'); }
<STRING_BEGIN> \\u{HexDigit}{HexDigit}{HexDigit}{HexDigit}	{
    try {
        int ch = Integer.parseInt(yytext().substring(2),16);
        sb.append((char)ch);
    }
    catch (Exception e) {
        throw new JsonParsingException(e.getMessage(), e.getCause(), new JsonLocation(yychar));
    }
}
<STRING_BEGIN> \\   { sb.append('\\'); }

"\""      {
    sb = new StringBuilder();
    yybegin(STRING_BEGIN);
}
{Int}   {
    parser.yylval = new ParserVal((int)Integer.valueOf(yytext()));
    return Parser.INT;
}
{Real} {
    parser.yylval = new ParserVal((double)Double.valueOf(yytext()));
    return Parser.REAL;
}
"true"  { return Parser.TRUE;}
"false" { return Parser.FALSE;}
"null"  { return Parser.NULL; }
"{"     { return '{'; }
"}"     { return '}'; }
"["     { return '['; }
"]"     { return ']'; }
","     { return ','; }
":"     { return ':'; }
{WhiteSpace}+   {}
.   {
        char[] a = new char[1];
        a[0] = yycharat(0);
        throw new JsonParsingException(
            String.format("Unexpected character: \\u%04x at position %d",
                    Character.codePointAt(a, 0), yychar),
            new JsonLocation(yychar));
    }
